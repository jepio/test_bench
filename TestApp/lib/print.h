#include <string>
#include <lib/print_export.h>

class PRINT_EXPORT Print {
public:
	Print(std::string msg);
	void printMsg();
private:
	std::string msg_;
};