#include "catch.hpp"
#include "factorial.hpp"

TEST_CASE("Factorial is calculated.", "[factorial]")
{
    REQUIRE( Factorial(0) == 1 );
    REQUIRE( Factorial(1) == 1 );
    REQUIRE( Factorial(2) == 2 );
    REQUIRE( Factorial(3) == 6 );
    REQUIRE( Factorial(10) == 3628800 );
}
