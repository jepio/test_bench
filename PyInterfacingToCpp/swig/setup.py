from distutils.core import setup, Extension

example_module = Extension(
    "_kernel", sources=["kernel.i", "kernel.cpp"],
    extra_compile_args=["-g", "-std=c++11"],
    swig_opts=["-c++"]
)

setup(name="kernel",
      version="0.1",
      author="Jepio",
      description="""SWIG test""",
      ext_modules=[example_module],
      py_modules=["example"],
      )
