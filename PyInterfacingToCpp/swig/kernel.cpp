#include "kernel.h"
#include <cmath>

CSKernel::CSKernel(double h_) : h{h_}
{
    if (h < 1e-8)
        h = 1e-8;
}
DerivCSKernel::DerivCSKernel(double h_) : h{h_}
{
    if (h < 1e-8)
        h = 1e-8;
}
double CSKernel::operator()(double r) const
{
    constexpr double k = 2.0 / 3.0;

    double q = fabs(r) / (2.0 * h);
    double value = 0.0;

    if (q > 1) {
        return value;
    } else if (q < 0.5) {
        value = 1 - (6 * q * q) + (6 * q * q * q);
    } else {
        value = 2 * pow((1 - q), 3);
    }
    return k / h * value;
}

double DerivCSKernel::operator()(double r) const
{
    constexpr double k = 2.0 / 3.0;

    double q = fabs(r) / 2.0 / h;
    double value = 0;

    if (fabs(q) < 0.5) {
        value = -12 * q + 18 * q * q;
    } else if (fabs(q) < 1) {
        value = -6 * pow((1 - q), 2);
    }
    if (r < 0)
        value *= -1;
    return k / (2 * h * h) * value;
}
