#pragma once

#include <vector>

// Cubic Spline Kernel
class CSKernel {
public:
    CSKernel(double h_);
    double operator()(double r) const;
    double h;
};

// Derivative Cubic Spline Kernel
class DerivCSKernel {
public:
    DerivCSKernel(double h_);
    double operator()(double r) const;
    double h;
};

