#pragma once

#include <vector>

// Cubic Spline Kernel
struct CSKernel {
    CSKernel(double h_);
    double operator()(double r) const;
    double h;
};

// Derivative Cubic Spline Kernel
struct DerivCSKernel {
    DerivCSKernel(double h_);
    double operator()(double r) const;
    double h;
};

inline CSKernel::CSKernel(double h_) : h{h_}
{
    if (h < 1e-8)
        h = 1e-8;
}
inline DerivCSKernel::DerivCSKernel(double h_) : h{h_}
{
    if (h < 1e-8)
        h = 1e-8;
}
