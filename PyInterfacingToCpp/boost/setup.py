from distutils.core import setup, Extension

extension_name = "test"
extension_version = "0.1"

src = ["kernel.cpp", "test.cpp"]

ext = Extension(extension_name, src,
                extra_compile_args=["-std=c++11"],
                libraries=["boost_python-2.7"])

setup(name=extension_name, version=extension_version, ext_modules=[ext])
