// all includes:
// #include <boost/python.hpp>

// explicit includes (should speedup compilation):
#include <boost/python/class.hpp>
#include <boost/python/def.hpp>
#include <boost/python/module.hpp>
#include "kernel.h"

const char* greet()
{
    return "hello, world";
}

BOOST_PYTHON_MODULE(test)
{
    using namespace boost::python;
    def("greet", greet);
    class_<CSKernel>("CSKernel", init<double>())
        .def_readwrite("h", &CSKernel::h)
        .def("__call__", &CSKernel::operator());
}


