#include "test.h"
#include <stdlib.h>

int square(int num)
{
    return num * num;
}

int negate(int num)
{
    return -num;
}

struct x {
    int first, second;
};

struct x *make_x(int first, int second)
{
    struct x *xx = malloc(sizeof *xx);
    *xx = (struct x){ first, second };
    return xx;
}

void destroy_x(struct x *xx)
{
    free(xx);
}
