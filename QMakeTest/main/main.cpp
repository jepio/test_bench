#include <string>

int double_value(int value);
void print_value(int value);

int main(int argc, char *argv[])
{
    if (argc != 2)
        return -1;
    int value = std::stoi(argv[1]);
    value = double_value(value);
    print_value(value);
}
