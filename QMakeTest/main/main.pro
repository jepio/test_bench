include(../project.pri)

TEMPLATE = app
SOURCES += main.cpp

LIBS += -L../printing -lprinting -L../math -lmath

QMAKE_RPATHDIR = $(PWD)/../printing $(PWD)/../math

