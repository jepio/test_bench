#include <limits>
#include <stdexcept>

int double_value(int value)
{
    if (value > (std::numeric_limits<int>::max() / 2))
        throw std::domain_error{"Number is too big"};
    return 2 * value;
}
