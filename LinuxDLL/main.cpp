#include <iostream>
#include "DynamicLib.h"

int main()
{
	using std::cout;
	using std::cerr;
	cout << "Demo:\n";
	DynamicLib lib("./libmodule.so");
	using hello_fun_t = void(void);
	auto hello_fun = lib.get_function<hello_fun_t>("hello_fun");
	hello_fun();
}
