#pragma once
#include <dlfcn.h>

class DynamicLib {
public:
	DynamicLib(std::string lib_name, int flag = RTLD_LAZY);
	~DynamicLib();
	template <typename Fun_t>
	Fun_t* get_function(std::string fun_name)
	{
		return reinterpret_cast<Fun_t*>(
		    dlsym(handle, fun_name.c_str()));
	}

private:
	void* get_symbol(const std::string& fun_name);
	void* handle;
};
