#include <string>
#include <stdexcept>
#include "DynamicLib.h"

DynamicLib::DynamicLib(std::string lib_name, int flag)
    : handle(dlopen(lib_name.c_str(), flag))
{
	if (!handle) {
		throw std::runtime_error(dlerror());
	}
	dlerror();
}

DynamicLib::~DynamicLib() { dlclose(handle); }

void* DynamicLib::get_symbol(const std::string& fun_name)
{
	auto function = dlsym(handle, fun_name.c_str());
	auto error_str = dlerror();
	if (error_str) {
		throw std::runtime_error(error_str);
	}
	return function;
}
