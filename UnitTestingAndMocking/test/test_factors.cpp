#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include "factors.h"

using namespace std;

struct fixture {
    vector<int> expected;
    vector<int> actual;
    void setup(vector<int> const& ex) { expected = ex; }
    void prime_factors(int n) { actual = ::prime_factors(n); }
    ~fixture()
    {
        BOOST_REQUIRE_EQUAL_COLLECTIONS(begin(expected), end(expected),
                                        begin(actual), end(actual));
    }
};

BOOST_FIXTURE_TEST_CASE(one_yields_empty, fixture) { prime_factors(1); }

BOOST_FIXTURE_TEST_CASE(two_yields_two, fixture)
{
    setup({2});

    prime_factors(2);
}

BOOST_FIXTURE_TEST_CASE(three_yield_3, fixture)
{
    setup({3});

    prime_factors(3);
}

BOOST_FIXTURE_TEST_CASE(four_yield_2_2, fixture)
{
    setup({2, 2});

    prime_factors(4);
}

BOOST_FIXTURE_TEST_CASE(six_yield_2_3, fixture)
{
    setup({2, 3});

    prime_factors(6);
}

BOOST_FIXTURE_TEST_CASE(eight_yields_2_2_2, fixture)
{
    setup({2, 2, 2});

    prime_factors(8);
}

BOOST_FIXTURE_TEST_CASE(nine_yields_3_3, fixture)
{
    setup({3, 3});

    prime_factors(9);
}
