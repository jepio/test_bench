#include "factors.h"

using namespace std;

vector<int> prime_factors(int number)
{
    vector<int> primes;

    int candidate = 2;
    while (number > 1) {
        for (; number % candidate == 0; number /= candidate) {
            primes.push_back(candidate);
        }
        ++candidate;
    }
    return primes;
}
