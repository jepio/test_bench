Unit Testing and Mocking
=========================

Based on the tutorial given at **cppnow 2014**, to be found on [boostcons
github][boostcon]. Requires CMake, Boost and Turtle ([header-only][turtle]).

[boostcon]: https://github.com/boostcon/cppnow_presentations_2014/tree/master/files/test_driven
[turtle]: http://turtle.sourceforge.net/
