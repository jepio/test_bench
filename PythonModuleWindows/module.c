#include <stdio.h>
#include <Python.h>

static PyObject *spam_system(PyObject *self, PyObject *args)
{
    const char *command;
    if (!PyArg_ParseTuple(args, "s", &command))
        return NULL;
    
    printf("%s\n", command);

    return Py_BuildValue("i", 25);
}

static PyMethodDef methods[] = {
    {"first", spam_system, METH_VARARGS, "Print a string."},
    {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC init_mod(void)
{
    (void)Py_InitModule("_mod", methods);
}

