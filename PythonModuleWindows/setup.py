from distutils.core import setup, Extension

mod = Extension("_mod", sources=["module.c"])

setup(name="Module", 
      version="1.0",
      description="Test module",
      ext_modules=[mod])
