CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

project(PyModule)
find_package(PythonLibs REQUIRED)

add_library(_mod SHARED module.c)
include_directories(${PYTHON_INCLUDE_DIRS})
target_link_libraries(_mod ${PYTHON_LIBRARIES})

set_target_properties(_mod PROPERTIES
    PREFIX "")
if(WIN32)
    set_target_properties(_mod PROPERTIES
        SUFFIX ".pyd")
endif()
